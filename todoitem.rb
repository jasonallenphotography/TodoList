class TodoItem
  attr_accessor :description, :is_complete

  def initialize(options = {})
    @description = options[:description]
    @is_complete = options[:is_complete] || "false"
  end

  def to_s
    "#{description}"
  end
end