class List
  attr_accessor :todo_items

  def initialize
    @todo_items = Parse.parse_csv_to_todoitem("sample_list.csv")
  end

  def list_out
    @todo_items.each_with_index do |todo, idx|
      "#{idx}" + " todo.to_s"
    end
  end

  def add_new_task_to_list(new_task)
    @todo_items << TodoItem.new(description: new_task)
  end

  def remove_task_from_list(id)
    @todo_items.delete_at(id.to_i - 1)
  end

  def complete(id)
    @todo_items[id.to_i - 1 ].is_complete = "true"
  end

end
