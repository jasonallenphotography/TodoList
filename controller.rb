class Controller
  def initialize(filename)
    @filename = filename
    @newlist = List.new
    run
  end

  def run
    case ARGV[0]
      when "add"
        @newlist.add_new_task_to_list(ARGV[1..-1].join(' '))
        Parse.rewrite_file(@newlist.todo_items, @filename)
        View.display_list(@newlist.list_out)
      when "complete"
        @newlist.complete(ARGV[1])
        Parse.rewrite_file(@newlist.todo_items, @filename)
        View.display_list(@newlist.list_out)
      when "remove"
        @newlist.remove_task_from_list(ARGV[1])
        Parse.rewrite_file(@newlist.todo_items, @filename)
        View.display_list(@newlist.list_out)
      else
        View.display_list(@newlist.list_out)
    end
  end

  def list_out
    @view.display_list
  end

end
