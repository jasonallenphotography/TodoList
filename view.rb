module View

  def self.display_list(todo_items)
    system 'clear'
    puts " Done ?  |  Stuff to do:"
    puts "=".center(50, "=")
    todo_items.each_with_index do |todo, index|
      puts "#{index + 1}. #{todo.is_complete == 'true' ? '[X]' : '[ ]'}   |  #{todo.description}"
    end
    puts "=".ljust(50, "=")
    puts "  That's all there is to do! Get started!"
    puts
    puts "  If you'd like to add a new item, write add after"
    puts "  the command line application, followed by the"
    puts "  'task to complete' in quores"
    puts "  i.e. $/...todo.rb add 'Get groceries!' "
    puts
    puts "  To complete a task, write complete followed"
    puts "  by the list number of the task to complete."
    puts "  i.e. $/...todo.rb complete 1 "
    puts
    puts "  To remove a task, write remove followed"
    puts "  by the task to remove."
    puts "  i.e. $/...todo.rb remove 1' "

  end
end
