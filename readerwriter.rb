module Parse
  HEADERS = ["description","is_complete"]
  def self.parse_csv_to_todoitem(file)
    CSV.foreach(file, headers: true, header_converters: :symbol).map do |row|
      TodoItem.new(row)
      end
  end

  def self.rewrite_file(data_to_save, file)
    CSV.open(file, "w+") do |csv|
      csv << HEADERS
    end

    data_to_save.each do |list_item|
      new_csv_row = [list_item.description, list_item.is_complete]
      CSV.open(file, "a+") do |csv|
        csv << new_csv_row
      end
    end
  end

end
